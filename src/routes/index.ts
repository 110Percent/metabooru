import postsRoute from "./posts";
import postRoute from "./post";
import fileRoute from "./file";
import previewRoute from "./preview";
import aboutRoute from "./about";
import termsRoute from "./terms";
import homeRoute from "./home";
import fp from "fastify-plugin";
import { FastifyInstance, FastifyPluginOptions, FastifyError } from "fastify";

export default fp(
  (
    fastify: FastifyInstance,
    opts: FastifyPluginOptions,
    next: (error?: FastifyError) => void
  ): void => {
    fastify.get("/", homeRoute);
    fastify.get("/about", aboutRoute);
    fastify.get("/terms", termsRoute);
    fastify.get("/posts", postsRoute);
    fastify.get("/post/:md5", postRoute);
    fastify.get("/file/:site/*", fileRoute);
    fastify.get("/preview/:site/*", previewRoute);
    next();
  }
);
