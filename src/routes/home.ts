import { FastifyRequest, FastifyReply } from "fastify";
import { redis } from "../core/cache";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  let visits = 1;
  if (await redis.get("total_visits")) {
    visits = Number(await redis.get("total_visits")) + 1;
  }
  await redis.set("total_visits", visits);
  await reply.view("/templates/index.ejs", { visits });
};
