import { FastifyRequest, FastifyReply } from "fastify";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  await reply.view("/templates/about.ejs");
};
