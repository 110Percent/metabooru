import { FastifyRequest, FastifyReply } from "fastify";
import { abcache } from "../core/cache";
import { getFile } from "../core/proxy";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  const cachePath = `${(<any>req.params)?.site}/thumbnail/${
    (<any>req.params)["*"]
  }`;
  if (await abcache.has(cachePath)) {
    const cached = await abcache.get(cachePath);
    const type = await abcache.get(`${cachePath}/type`);
    await reply.type(type).send(Buffer.from(cached.item.data));
  } else {
    const res = await getFile(
      (<any>req.params)["*"],
      (<any>req.params)?.site,
      "thumbnail"
    );
    if (res === null) {
      reply.view("/templates/404.ejs", {
        title: "File not found",
        desc:
          "The file was not found on the requested server. This is likely an error on our part. Sorry about that.",
      });
    } else {
      abcache.set(cachePath, res.body, 86400000).then(() => {
        console.log(`Cached ${cachePath}`);
      });
      abcache.set(`${cachePath}/type`, res.type, 86400000);
      reply.type(res.type).send(res.body);
    }
  }
};
