import { getPosts } from "../core/fetch";
import { boorus } from "../core/booru_list";
import { FastifyRequest, FastifyReply } from "fastify";
import { redis } from "../core/cache";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  console.log(`Fetching posts for query ${(<any>req.query).q || ""}`);
  const posts = await getPosts((<any>req.query).q || "");
  const tags: { [key: string]: number } = {};
  for (const post of posts) {
    const lTags = post.tags.split(" ");
    for (const tag of lTags) {
      if (tags[tag]) {
        tags[tag]++;
      } else {
        tags[tag] = 1;
      }
    }
  }
  console.log("Sorting tags");
  let numTags: [string, number][] = [];
  for (const tag in tags) {
    numTags.push([tag, tags[tag]]);
  }
  const searchTags = (<any>req.query).q ? (<any>req.query).q.split(" ") : [];
  numTags = numTags
    .sort((a: [string, number], b: [string, number]) => b[1] - a[1])
    .filter((a: [string, number]) => !searchTags.includes(a[0]))
    .slice(0, 50);
  console.log("Sending reply");
  await reply.view("/templates/posts.ejs", {
    posts /*: posts.splice(0, 75) */,
    query: (<any>req.query).q || "",
    numTags,
    boorus: Object.keys(boorus),
  });
  let visits = 1;
  if (await redis.get("total_visits")) {
    visits = Number(await redis.get("total_visits")) + 1;
  }
  await redis.set("total_visits", visits);
};
