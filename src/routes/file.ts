import { abcache } from "../core/cache";
import { getFile } from "../core/proxy";
import { FastifyReply, FastifyRequest } from "fastify";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  const name = (<any>req.params)["*"].split("/")[
    (<any>req.params)["*"].split("/").length - 1
  ];
  const directory = (<any>req.params)["*"].includes("/")
    ? (<any>req.params)["*"]
        .split("/")
        .splice(0, (<any>req.params)["*"].split("/").length - 1)
        .join("/")
    : null;
  const cachePath = `${(<any>req.params)?.site}/file/${directory}/${name}`;
  if (await abcache.has(cachePath)) {
    const cached = await abcache.get(cachePath);
    const type = await abcache.get(`${cachePath}/type`);
    await reply.type(type.item).send(Buffer.from(cached.item.data));
  } else {
    const res = await getFile(name, (<any>req.params)?.site, "file", directory);
    if (res === null) {
      reply.type("image/jpeg").sendFile("img/404_raw.jpg");
    } else {
      abcache.set(cachePath, res?.body, 86400000).then(() => {
        console.log(`Cached ${cachePath}`);
      });
      abcache.set(`${cachePath}/type`, res?.type, 86400000);
      reply.type(res?.type).send(res?.body);
    }
  }
};
