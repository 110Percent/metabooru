import { FastifyRequest, FastifyReply } from "fastify";
import { getPost } from "../core/fetch";
import { redis } from "../core/cache";

export default async (
  req: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  console.log(`Fetching post ${(<any>req.params)?.md5}`);
  const post = await getPost((<any>req.params)?.md5);
  if (Object.keys(post).length > 0) {
    await reply.view("/templates/post.ejs", { post });
  } else {
    await reply.view("/templates/404.ejs", {
      title: "File not found",
      desc:
        "The file you were looking for was not found on any booru we searched. You probably entered the wrong URL.",
    });
  }
  let visits = 1;
  if (await redis.get("total_visits")) {
    visits = Number(await redis.get("total_visits")) + 1;
  }
  redis.set("total_visits", visits);
};
