import toml from "@iarna/toml";
import fs from "fs";
import path from "path";

const tomlFile = (fPath: string) => {
  return toml.parse(
    fs.readFileSync(path.join(__dirname, "..", "..", "config", fPath), {
      encoding: "utf-8",
    })
  );
};

const booruTypes = tomlFile("booru_types.toml");
const boorus = tomlFile("boorus.toml");

export { booruTypes, boorus };
