import Redis from "ioredis";
// @ts-ignore
import abs from "abstract-cache";

const redis = new Redis({ host: "127.0.0.1" });

const abcache = abs({
  useAwait: true,
  driver: {
    name: "abstract-cache-redis", // must be installed via `npm install`
    options: { client: redis },
  },
});

export { abcache, redis };
