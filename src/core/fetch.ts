import request, { Response } from "superagent";

import { boorus, booruTypes } from "./booru_list";

const template = new RegExp("{([a-zA-z0-9]+)}");

const ratings: { [key: string]: string } = {
  s: "safe",
  q: "questionable",
  e: "explicit",
};

export const getPosts = async (query: string): Promise<any[]> => {
  console.log("Getting posts");
  const dataPromises: Promise<any>[] = [];
  console.log("Building promises");
  for (const [site, props] of Object.entries(boorus)) {
    dataPromises.push(multiPostData(site, props, query));
  }
  const data: any[] = await Promise.all(dataPromises);
  console.log("Data received");
  let returned: any[] = [];
  for (const obj of data) {
    if (obj !== null) {
      returned = [...returned, ...obj];
    }
  }
  returned = returned.sort((a: any, b: any) =>
    a.updateStamp > b.updateStamp ? -1 : 1
  );
  let i = 0;
  while (i < returned.length) {
    const indexed =
      returned[returned.findIndex((post: any) => post.md5 === returned[i].md5)];
    if (indexed === returned[i]) {
      i++;
    } else {
      indexed.sites.push(returned[i].sites[0]);
      returned.splice(i, 1);
    }
  }
  return returned;
};

async function multiPostData(site: string, props: any, query: string) {
  const typeProps: any = booruTypes[(<any>props).type];
  if (typeProps.metaQuery) {
    for (const key in typeProps.metaQuery) {
      query = query.replace(`${key}:`, `${typeProps.metaQuery[key]}:`);
    }
  }
  const url = `${(<any>props).urls.base}${
    typeProps.endpoints.postsTags
  }`.replace("%", encodeURIComponent(query));
  console.log(`Fetching data for ${query} from ${site}`);
  let res: Response;
  try {
    res = await request.get(url).send();
  } catch (err) {
    console.error(err);
    return null;
  }
  if (res.body.constructor === Object && Object.keys(res.body).length < 1) {
    try {
      res.body = JSON.parse(res.text);
    } catch (err) {
      return null;
    }
  }
  if (
    res.body.length < 1 ||
    Object.keys(res.body).length < 1 ||
    ("success" in res.body && res.body.success === false)
  ) {
    return null;
  }
  const body: any[] = [];
  res.body.forEach((post: any) => {
    if (!post.file_url) {
      return;
    }
    const toReturn = {
      ...post,
      sites: [site],
      updateStamp:
        Number(new Date(post[typeProps.dateStamp.field])) *
        typeProps.dateStamp.multi,
    };
    for (const field in typeProps.fields) {
      toReturn[field] = post[typeProps.fields[field]];
    }
    if ((<any>props).type === "danbooru") {
      const split = toReturn.file_url.split("/");
      toReturn.md5 = split[split.length - 1].split(".")[0];
    }
    if (!toReturn.directory) {
      toReturn.thumbDirectory = `${toReturn.md5.substring(
        0,
        2
      )}/${toReturn.md5.substring(2, 4)}`;
    }
    let thumbPath = typeProps.thumbnail;
    let match = template.exec(thumbPath);
    while (match !== null) {
      thumbPath = thumbPath.replace(match[0], toReturn[match[1]]);
      match = template.exec(thumbPath);
    }
    toReturn.thumbPath = thumbPath;
    body.push(toReturn);
  });
  console.log(`Fetched data from ${site}`);
  console.log(body);
  return body;
}

export const getPost = async (
  hash: string
): Promise<{ [key: string]: any }> => {
  let data: { [key: string]: any } = {};
  const dataPromises: Promise<any>[] = [];
  for (const [site, props] of Object.entries(boorus)) {
    dataPromises.push(postData(site, props, hash));
  }
  const dataList = await Promise.all(dataPromises);
  const sources: string[] = [];
  let rating = "safe";
  for (const result of dataList) {
    if (
      result === null ||
      Object.keys(result.body).length === 0 ||
      ("success" in result.body && result.body.success === false)
    ) {
      continue;
    }
    if (result.body.source) {
      const source = result.body.source.replace("http://", "https://");
      if (!sources.includes(source)) {
        sources.push(source);
      }
    }
    if (result.body.rating in ratings) {
      if (
        Object.keys(ratings).indexOf(result.body.rating) >
        Object.values(ratings).indexOf(rating)
      ) {
        rating = ratings[result.body.rating];
      }
    } else if (Object.values(ratings).includes(result.body.rating)) {
      if (
        Object.values(ratings).indexOf(result.body.rating) >
        Object.values(ratings).indexOf(rating)
      ) {
        rating = result.body.rating;
      }
    }
    data = { ...data, ...result.body };
    if (!data.viewURLs) {
      data.viewURLs = [result.catUrl];
    } else {
      data.viewURLs.push(result.catUrl);
    }
  }
  if (data.tags) {
    data.tags = data.tags
      .filter((t: string, i: number) => data.tags.indexOf(t) === i)
      .sort((a: string, b: string) => (a > b ? 1 : -1));
  }

  data = { ...data, sources, rating };

  return data;
};

async function postData(site: string, props: any, hash: string) {
  const typeProps: any = booruTypes[(<any>props).type];
  let catUrl: [string, string] = ["", ""];
  const url = `${(<any>props).urls.base}${
    typeProps.endpoints.postHash
  }`.replace("%", encodeURIComponent(hash));
  const res = await request
    .get(url)
    .ok(() => true)
    .send();
  if (res.body.constructor === Object && Object.keys(res.body).length < 1) {
    try {
      res.body = JSON.parse(res.text);
    } catch (err) {
      return null;
    }
  }
  console.log(`Fetched post data from ${site}`);
  let body: { [key: string]: any } = {};
  if (
    (res.body.constructor === Array && res.body.length > 0) ||
    (typeof res.body === "object" && Object.values(res.body).length > 0)
  ) {
    let tags: string[] = [];
    body = typeProps.isList.postHash ? res.body[0] : res.body;
    if ((<any>props).type === "danbooru") {
      if (!("success" in res.body) || res.body.success === true) {
        const postUrl = `${(<any>props).urls.base}${
          typeProps.endpoints.postID
        }`.replace("%", encodeURIComponent(body.id));
        const postRes = await request
          .get(postUrl)
          .ok(() => true)
          .send();
        if (!(typeProps.fields.tags in res.body)) {
          tags = postRes.body[typeProps.fields.tags].split(" ");
        } else {
          tags = body[typeProps.fields.tags].split(" ");
        }
        body = { ...body, ...postRes.body };
      } else {
        return null;
      }
    } else {
      tags = body[typeProps.fields.tags].split(" ");
    }

    if (body.file_url && !body.md5) {
      const split = body.file_url.split("/");
      body.md5 = split[split.length - 1].split(".")[0];
    }

    body.site = site;
    for (const field in typeProps.fields) {
      body[field] = body[typeProps.fields[field]];
    }
    body.tags = tags;
    catUrl = [
      (<any>props).name,
      `${(<any>props).urls.base}${typeProps.endpoints.postView}`.replace(
        "%",
        body.id
      ),
    ];
  }
  return { body, catUrl };
}
