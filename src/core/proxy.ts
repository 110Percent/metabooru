import request, { Response } from "superagent";

import { boorus } from "./booru_list";

export const getFile = async (
  name: string,
  site: string,
  type: string,
  directory?: string
): Promise<Response | null> => {
  let filename = name;
  const booru: any = boorus[site];
  if (booru.forceExt) {
    filename =
      name
        .split(".")
        .slice(0, name.split(".").length - 1)
        .join(".") +
      "." +
      booru.forceExt;
  }
  let prepend = booru.urls[type];
  const hash = filename.split("_")[filename.split("_").length - 1];
  const tDir = directory || `${hash.substring(0, 2)}/${hash.substring(2, 4)}`;
  let url = `${prepend}/${filename}`;

  try {
    return await request.get(url).send();
  } catch (err) {
    console.error(`Could not find ${url}`);
  }
  try {
    url = `${prepend}/${tDir}/${filename}`;
    return await request.get(url).send();
  } catch (err) {
    console.error(`Could not find ${url}`);
  }
  if (type === "file" && booru.urls.file_backup) {
    prepend = booru.urls.file_backup;
    try {
      url = `${prepend}/${filename}`;
      return await request.get(url).send();
    } catch (err) {
      console.error(`Could not find ${url}`);
    }
    try {
      url = `${prepend}/${tDir}/${filename}`;
      return await request.get(url).send();
    } catch (err) {
      console.error(`Could not find ${url}`);
    }
  }
  return null;
};
