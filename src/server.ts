import ejs from "ejs";
import fast from "fastify";
import fastStatic from "fastify-static";
import pov from "point-of-view";
import path from "path";
import routes from "./routes";

const fastify = fast({ logger: false });

fastify
  .register(pov, {
    engine: { ejs },
  })
  .register(fastStatic, {
    root: path.join(__dirname, "../public"),
  })
  .register(routes);

fastify.setNotFoundHandler((request, reply) => {
  reply.code(404).view("/templates/404.ejs", {
    title: "Page not found",
    desc:
      "That page doesn't exist. You either typed the wrong URL or clicked a broken link.",
  });
});

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000, "0.0.0.0");
    console.log("Started Metabooru server!");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
